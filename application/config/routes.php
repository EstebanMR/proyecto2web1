<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['principal'] = 'principal';
$route['login'] = 'principal/login';
$route['registrar'] = 'principal/signup';
$route['admin'] = 'admin';
$route['logout'] = 'principal/logout';
$route['admin/categorias'] = 'admin/verCategorias';
$route['admin/productos'] = 'admin/verProductos';
$route['admin/categoriasDel/(:num)'] = 'admin/delCat/$1';
$route['admin/categorias/(:num)'] = 'admin/upCat/$1';
$route['admin/categoriasCr'] = 'admin/crearCat';
$route['admin/categoriasUp'] = 'admin/updateCat';
$route['admin/productosDel/(:num)'] = 'admin/delPro/$1';
$route['admin/productos/(:num)'] = 'admin/upPro/$1';
$route['admin/productosCr'] = 'admin/crearPro';
$route['admin/productosUp'] = 'admin/updatePro';
$route['selectCat'] = 'user/selectCat';
$route['user/mostrar_producto/(:num)'] = 'user/mostrar_producto/$1';
$route['user/agregar_carrito'] = 'user/agregar_carrito';
$route['user/carrito'] = 'user/ver_carrito';
$route['user/compras'] = 'user/ver_compras';
$route['user/delcarrito/(:num)'] = 'user/del_carrito/$1';
$route['user/checkout'] = 'user/checkout';
$route['user/compras'] = 'user/compras';
$route['user/vercompra/(:any)/(:any)/(:any)'] = 'user/ver_compra/$1/$2/$3';



$route['default_controller'] = 'principal';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
