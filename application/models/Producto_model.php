<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Producto_model extends CI_Model
    {
        public $id;
        public $codigo;
        public $nombre;
        public $descripcion;
        public $imagen;
        public $categoria;
        public $stock;
        public $precio;

        function __construct()
        {
            parent::__construct();  
        }

        /**
         * Actualiza un producto de la base de datos
         */
        public function actualizar($id, $codigo, $nombre, $descripcion, $imagen, $categoria, $stock, $precio)
        {
            $sql='UPDATE `productos` SET `codigo`="'.$codigo.'",`nombre`="'.$nombre.'",`descripcion`="'.$descripcion.'",`imagen`="'.$imagen.'",`categoria`='.$categoria.',`stock`='.$stock.',`precio`='.$precio.' WHERE id='.$id.'';
            try{
                $ejecutar=$this->db->query( $sql);

                if (!$ejecutar) {
                    return false;
                } else{
                    return true;
                }
            }catch(Exception $error){
                echo'Ha ocurrido un error =>'.$error;
            }    
        }

        /**
         * Crea un producto en la base de datos
         */
        public function crear($codigo, $nombre, $descripcion, $imagen, $categoria, $stock, $precio)
        {
            $data =array('codigo'=>$codigo, 'nombre'=>$nombre, 'descripcion'=>$descripcion,'imagen'=>$imagen, 'categoria'=>$categoria,'stock'=>$stock ,'precio'=>$precio);
            $sql='INSERT INTO `productos`(`codigo`, `nombre`, `descripcion`, `imagen`, `categoria`, `stock`, `precio`) VALUES ( ?,?,?,"?",?,?,?)';
            try{
                $ejecutar=$this->db->query( $sql, $data);

                if (!$ejecutar) {
                    return false;
                } else{
                    return true;
                }
            }catch(Exception $error){
                echo'Ha ocurrido un error =>'.$error;
            }  
        }

        /**
         * Elimina porductos de la base de datos
         */
        public function eliminar($id)
        {
            try{
                $this->db->where('id', $id);
                $ejecutar=$this->db->delete('Productos');

                if (!$ejecutar) {
                    return true;
                } else{
                    return false;
                }
            }catch(Exception $error){
                echo'Ha ocurrido un error =>'.$error;
            }  
        }

        /**
         * Realiza la consulta de la cantidad de compras realizadas todos los usuarios
         */
        public function cantVentas()
        {
            $sql = "SELECT SUM(cantidad) FROM `ventas`";
            try{
                $ejecucion=$this->db->query($sql);
                $res1 = $ejecucion->result();
                $cant=0;
                                            
                foreach ($res1[0] as $value) {
                    $cant = $value;
                }         
                
                return $cant;
                                            
            }catch(Exception $e) {
                echo 'Excepción capturada: '.$e;
            } 
        }

        /**
         * Realiza la consulta de la cantidad gastada por todos los usuarios
         */
        public function montoVentas()
        {
            $sql = "SELECT SUM(precio) FROM `ventas`";
            try{
                $ejecucion=$this->db->query($sql);
                $res1 = $ejecucion->result();
                                            
                $cant=0;
                                            
                foreach ($res1[0] as $value) {
                    $cant = $value;
                }         
                
                return $cant;                                          
                                            
            }catch(Exception $e) {
                echo 'Excepción capturada: '.$e;
            } 
        }

        /**
         * Trae todos los productos de la base de datos
         */
        public function getAll()
        {
            $script = "SELECT `id`, `codigo`, `nombre`, `descripcion`, `imagen`, `categoria`, `stock`, `precio` FROM `productos`";
                                    
            try{
                $ejecucion=$consulta=$this->db->query($script);
                $res = $ejecucion->result();
                $prod = array();     
                foreach ($res as $pro) {
                    $index = array();
                    foreach ($pro as $val) {
                        $index[]= $val;
                   }
                   $prod[]=$index;
                } 
                return $prod;
                                        
            }catch(Exception $e) {
                echo 'Excepción capturada: '.$e;
            }
        }

        /**
         * Consulta la cantidad de compras realizadas por el usuario actual
         */
        public function userProd($id)
        {
            $script = "SELECT SUM(cantidad) FROM `ventas` WHERE usuario = ?";
                                                            
            try{
                $ejecucion=$this->db->query($script, array('id'=>$id));
                $res = $ejecucion->result();
                                
                $cant=0;
                                            
                foreach ($res[0] as $value) {
                    $cant = $value; 
                }         
                
                return $cant;                    
                                                                
            }catch(Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
        }

        /**
         * consulta el monto gastado por el usuario actual
         */
        public function userMont($id)
        {
            $script = "SELECT SUM(precio) FROM `ventas` WHERE usuario=?";
                                                                
            try{
                $ejecucion=$this->db->query($script, array('id'=>$id));
                $res = $ejecucion->result();
                                    
                $cant=0;
                                            
                foreach ($res[0] as $value) {
                    $cant = $value;
                }         
                
                return $cant;                   
                                                                    
            }catch(Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
        }

        /**
         * Obtiene un producto por medio de su id
         */
        public function getPro($id)
        {
            $script = "SELECT `id`, `codigo`, `nombre`, `descripcion`, `imagen`, `categoria`, `stock`, `precio` FROM `productos`WHERE id=?";
                                      
        try{
            $ejecucion=$this->db->query($script, array('id'=>$id));
            $res = $ejecucion->result();
            
            $prod = array();   

            foreach ($res[0] as $pro) {
                $prod[]=$pro;
              
            } 
            return $prod;
                                            
        }catch(Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        } 
        }
    }
    
?>