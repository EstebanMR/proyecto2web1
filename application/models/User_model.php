<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class User_model extends CI_Model
    {

        public $id;
        public $nombre;
        public $apellidos;
        public $telefono;
        public $correo;
        public $direccion;
        public $contrasenna;
        public $admin;
        public $nusuario;
        public $us;

        function __construct()
        {
            parent::__construct();
            $this->load->database();
        }

        /**
         * Se encarga de verificar la existencia en la base de datos del usuario ingresado en el login
         */
        public function userExist($nom, $pass)
        {
            $consulta=$this->db->query("SELECT * FROM `usuarios` WHERE  usuario LIKE ? AND contrasenna LIKE ? ;", array($nom, $pass));
            $data=$consulta->result();
            if(!empty($data)){
                return true;
            }else{
                return false;
            } 
        }

        /**
         * Se encarga de buscar y traer los datos del usuario de la base de datos
         */
        public function setUsuario($nu, $cont)
        {
            $script="SELECT `id`, `nombre`, `apellido`, `tel`, `correo`, `direccion`, `contrasenna`, `admin`, `usuario` FROM `usuarios`WHERE  usuario like ? and contraseNna like ?;";
            try{
                $ejecucion=$this->db->query($script, array($nu, $cont));
                $usuariodb = $ejecucion->result();

                $this->id =$usuariodb[0]->id;
                $this->nombre=$usuariodb[0]->nombre;
                $this->apellidos=$usuariodb[0]->apellido;
                $this->telefono= $usuariodb[0]->tel;
                $this->correo=$usuariodb[0]->correo;
                $this->direccion =$usuariodb[0]->direccion;
                $this->contrasenna=$usuariodb[0]->contrasenna;
                $this->admin=$usuariodb[0]->admin;
                $this->nusuario= $usuariodb[0]->usuario;

                foreach ($usuariodb[0] as $value){
                    $this->us[] = $value;
                }

            }catch(Exception $e) {
                echo 'Excepción capturada: '.$e;
            }
        }

        /**
         * Se encarga de traer la cantidad de usuarios resgistrados en la base de datos
         */
        public function cantidad_usuarios()
        {
            $script="SELECT COUNT(nombre) FROM `usuarios` WHERE admin=0";
            try{
                $ejecucion=$this->db->query($script);
                $res = $ejecucion->result();
                $cant=0;
                                            
                foreach ($res[0] as $value) {
                    $cant = $value;
                }         
                
                return $cant;
                
            }catch(Exception $e) {
                echo 'Excepción capturada: '.$e;
            }
        }

        /**
         * Se encarga de crear un nuevo usuario en la base de datos
         */
        public function insert($nombre, $apellidos, $telefono, $correo, $direccion, $contrasenna, $nusuario)
        {
            try{
                $data=array('nombre'=>$nombre, 'apellido'=>$apellidos, 'tel'=>$telefono, 'correo'=>$correo, 'direccion'=>$direccion, 'contrasenna'=>$contrasenna,'admin'=>0, 'usuario'=>$nusuario);
                $ejecucion=$this->db->insert('usuarios',$data);
                if (!$ejecucion) {
                    return false;
                } else {
                    return true;
                }
                
                
            }catch(Exception $e) {
                echo 'Excepción capturada: '.$e;
            } 
        }
    }