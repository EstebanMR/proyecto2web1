<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class carrito_model extends CI_Model
    {
        public $usuario;
        public $fecha;
        public $producto;
        public $cantidad;
        public $descripcion;
        public $precio;

        function __construct()
        {
            parent::__construct();  
        }

        /**
         * Crea una entrada al carrito a la base de datos
         */
        public function crear()
        {
            $sql="INSERT INTO `carrito`(`usuario`, `fecha`, `producto`, `cantidad`, `descripcion`, `precio`) VALUES ($this->usuario,'$this->fecha',$this->producto,$this->cantidad,'$this->descripcion',$this->precio)";
            try{
                $ejecutar=$this->db->query($sql);

                if (!$ejecutar) {
                    return false;
                } else{
                    return true;
                }
            }catch(Exception $error){
                echo'Ha ocurrido un error =>', $error;
            } 
        }

        /**
         * Crea la venta de un producto en la base de datos
         */
        public function crear_venta($car)
        {
            $sql='INSERT INTO `ventas`(`usuario`, `fecha`, `producto`, `cantidad`, `descripcion`, `precio`) VALUES ('.$car[1].',"'.$car[2].'",'.$car[3].','.$car[4].',"'.$car[5].'",'.$car[6].')';
            try{
                $ejecutar=$this->db->query($sql);

                if (!$ejecutar) {
                    return false;
                } else{
                    return true;
                }
            }catch(Exception $error){
                echo'Ha ocurrido un error =>', $error;
            } 
        }

        /**
         * Elimina un producto del carrito en la base de datos
         */
        public function eliminar($id)
        {
            $sql="DELETE FROM `carrito` WHERE id = $id";
            try{
                $ejecutar=$this->db->query($sql);

                if (!$ejecutar) {
                    return false;
                } else{
                    return true;
                }
            }catch(Exception $error){
                echo'Ha ocurrido un error =>', $error;
            }  
        }

        /**
         * o
         * Obtiene los productos con su imagen de el carrito según el usuario de la base de datos
         */
        public function getAll($id)
        {
            $script = "SELECT c.id, c.usuario, c.fecha, c.producto, c.cantidad, c.descripcion, c.precio, p.imagen FROM carrito AS c INNER JOIN productos AS p WHERE c.producto= p.id AND c.usuario=$id";
                                    
            try{
                $ejecucion=$consulta=$this->db->query($script);
                $res = $ejecucion->result();
                $prod = array();     
                foreach ($res as $pro) {
                    $index = array();
                    foreach ($pro as $val) {
                        $index[]= $val;
                   }
                   $prod[]=$index;
                } 
                return $prod;
                                        
            }catch(Exception $e) {
                echo 'Excepción capturada: '.$e;
            }
        }

        /**
         * Se encarga de bajar el stock de un producto en base de datoscada por cada compra 
         */
        public function bajar_stock($id, $cant)
        {
            $sql="UPDATE `productos` SET `stock`= $cant WHERE id=$id";
            try{
                $ejecutar=$this->db->query( $sql);

                if (!$ejecutar) {
                    return false;
                } else{
                    return true;
                }
            }catch(Exception $error){
                echo'Ha ocurrido un error =>'.$error;
            }    
        }

        /**
         * Obtiene de la base de datos todas las compras de un usuario 
         */
        public function get_ventas($id)
        {
            $script = "SELECT `id`, `usuario`, `fecha`, `producto`, `cantidad`, `descripcion`, `precio` FROM `ventas` WHERE usuario=$id";
                                    
            try{
                $ejecucion=$consulta=$this->db->query($script);
                $res = $ejecucion->result();
                $prod = array();     
                foreach ($res as $pro) {
                    $index = array();
                    foreach ($pro as $val) {
                        $index[]= $val;
                   }
                   $prod[]=$index;
                } 
                return $prod;
                                        
            }catch(Exception $e) {
                echo 'Excepción capturada: '.$e;
            } 
        }
    }
    
?>