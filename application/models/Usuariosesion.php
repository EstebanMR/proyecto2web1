<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Usuariosesion extends CI_Model{

        /**
         * Crea la sesión del usuario
         */
        public function _constructor()
        {
            session_start();
        }

        /**
         * Se encarga de obtener los datos del usuario y guardarlos en la sesion
         */
        public function cambiarUsuarioActual($usuario)
        {
            $_SESSION['usuario'] = $usuario;
        }

        /**
         * Devuelve los datos del usuario guardados en la sesión
         */
        public function darUsuarioActual()
        {
            return $_SESSION['usuario'];
        }

        /**
         * Se encarga de destruir la sesión
         */
        public function cerrarSesion()
        {
            session_unset();
            session_destroy();
            header('location: principal');
            
        }
    }
?>