<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class categorias_model extends CI_Model
    {
        public $id;
        public $nombre;
        
        function __construct()
        {
            parent::__construct();  
        }

        /**
         * Actualiza el nombre de una categoría en la base de datos por medio del id 
         */
        public function actualizar($id, $nom)
        {
            try{
                $ejecucion=$this->db->query("UPDATE `categorias` SET `nombre` = ? WHERE id=?;", array('nombre'=>$nom, 'id'=>$id));

                if (!$ejecucion) {
                    return false;
                } else{
                    return true;
                }
            }catch(Exception $error){
                echo'Ha ocurrido un error =>', $error;
            }    
        }

        /**
         * Se encarga de crear una categoría en la base de datos
         */
        public function crear($nom)
        {
            try{
                $ejecucion=$this->db->insert('categorias',array('nombre'=>$nom));
                if (!$ejecucion) {
                    return false;
                } else {
                    return true;
                }
                
                
            }catch(Exception $e) {
                echo 'Excepción capturada: '.$e;
            } 
        }

        /**
         * Se encarga de eliminar una categoría de la base de datos por medio de su id 
         */
        public function eliminar($id)
        {
            try{
                $this->db->where('id', $id);
                $ejecutar=$this->db->delete('categorias');

                if (!$ejecutar) {
                    return true;
                } else{
                    return false;
                }
            }catch(Exception $error){
                echo'Ha ocurrido un error =>', $error;
            }  
        }

        /**
         * Obtiene todas las categorías presentes en la base de datos
         */
        public function getAll()
        {
            $script = "SELECT `id`, `nombre` FROM categorias";
            try{
                $ejecucion=$this->db->query($script);
                $res = $ejecucion->result();
                $categorias = array();     
                foreach ($res as $cat) {
                    $index = array();
                    foreach ($cat as $val) {
                        $index[]= $val;
                   }
                   $categorias[]=$index;
                } 
                
                return $categorias;
                                        
            }catch(Exception $e) {
                echo 'Excepción capturada: '.$e;
            }
                               
        }
    } 
    
?>