<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('user_model');
		$this->load->model('usuariosesion');
		$this->load->library('form_validation');
		$this->load->helper('url', 'form');
	}

	/**
	 * Se encarga de validar si hay un usuario logeado o no y si este es administrador o no y carga las diferentes vistas o 
	 * coontroladores que correspondan
	 */
	public function index()
	{
		$this->usuariosesion->_constructor();
		if (isset($_SESSION['usuario'])) {
			$array = $this->usuariosesion->darUsuarioActual();
			$this->user_model->setUsuario($array->nusuario, $array->contrasenna);
			if($this->user_model->admin==1){
				header('location: admin'); 
			}else if ($this->user_model->admin==0) {
				header('location: user'); 
			}
		} else {
			$this->load->view('login');
		}
	}

	/**
	 * Toma los datos ingresados en el inicio de sesión  y realiza el inicio de sesión y redirecciona según el nivel de acceso del administrador
	 */
	public function login()
	{
		if ($this->input->post("enviar")) {
			try {
				$nom = $this->input->post("nombre");
				$pass = $this->input->post("contra");
				if ($this->user_model->userExist($nom, $pass)) {
					$this->user_model->setUsuario($nom, $pass);
					$this->usuariosesion->_constructor();
					$this->usuariosesion->cambiarUsuarioActual($this->user_model);
					if($this->user_model->admin==1){
						header('location: admin'); 
					}else if ($this->user_model->admin==0) {
						header('location: user');
					}
				} else {
					$sms= array('errorLogin'=>"El usuario no existe, por favor intente de nuevo");
					$this->load->view('login', $sms);
				}
				
			} catch (Exception $ex) {
				$this->load->view("login", array('errorLogin' => $ex));
			}
		}elseif ($this->input->post("registro")) {
			$this->load->view('signup');
		}
	}

	/**
	 * se encarga de tomar los datos del signup y registrar al usuario 
	 */
	public function signup()
	{
		if ($this->input->post("enviar")) {
			try {
				$nom = $this->input->post("nombre");
				$ap = $this->input->post("apellidos");
				$tel = $this->input->post("tel");
				$correo = $this->input->post("correo");
				$dir = $this->input->post("dir");
				$nu = $this->input->post("nombreu");
				$contra = $this->input->post("contra");
				
				if (!$this->user_model->userExist($nu, $contra)) {
					try {
						$res=$this->user_model->insert($nom, $ap, $tel, $correo, $dir, $contra, $nu);
						if ($res) {
							$this->load->view('login');
						} else{
							$this->load->view("signup", array('errorLogin' => "No se insertó el usuario, por favor intente de nuevo"));
						}
						
					} catch (Exception $ex) {
						$this->load->view("signup", array('errorLogin' => $ex));
					}
				} else {
					$sms= array('errorLogin'=>"El usuario a crear ya existe");
					$this->load->view('signup', $sms);
				}
				
			} catch (Exception $ex) {
				$this->load->view("signup", array('errorLogin' => $ex."Por favor revise los datos"));
			}
		}else {
			$this->load->view("signup", array('errorLogin' =>"Presione enviar"));
		}
	}

	/**
	 * Cierra sesión 
	 */
	public function logout()
	{
		$this->usuariosesion->_constructor();
		if (isset($_SESSION['usuario'])) {
			$this->usuariosesion->cerrarSesion();
		}else {
			echo "no hay sesion";
		}
		
	}
}
