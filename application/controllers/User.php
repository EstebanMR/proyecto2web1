<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
        $this->load->model('user_model');
		$this->load->model('producto_model');
		$this->load->model('categorias_model');
		$this->load->model('carrito_model');
		$this->load->model('usuariosesion');
		$this->load->library('form_validation');
		$this->load->helper('url', 'form');
		$this->usuariosesion->_constructor();
	}

	/**
	 * Esta función se encarga de primeramente verificar su el usuario no es administrador, si lo es se devuelve al controller principal,
	 * despues debe cargar la vista de los usuarios con sus datos para el dashboard, categorias y los producto 
	 */
	public function index()
	{
		
		if (isset($_SESSION['usuario'])) {
			$array = $this->usuariosesion->darUsuarioActual();
			$this->user_model->setUsuario($array->nusuario, $array->contrasenna);
			if($this->user_model->admin==0){
                /* $n1=$this->user_model->cantidad_usuarios();
                $n2=$this->producto_model->cantVentas();
				$n3=$this->producto_model->montoVentas(); */
				$this->load->view('user' , array('nom'=> $this->user_model->nombre." ".$this->user_model->apellidos ,'productos'=>$this->producto_model->getAll(), 'categorias'=>$this->categorias_model->getAll(), 'cant_pro'=>$this->producto_model->userProd($this->user_model->id), 'cant_compras'=>$this->producto_model->userMont($this->user_model->id)));
			}else if ($this->user_model->admin==1) {
				header('location: principal');
			}
		} else {
			$this->load->view('login');
		}
	}

	/**
	 * Toma la categoría seleccionada por el usuario en la vista de usuarios para cargar los productos de dicha categoría 
	 */
    public function selectCat()
    {
        if (isset($_POST['cat'])) {
            $this->load->view('user' , array('nom'=> $this->user_model->nombre." ".$this->user_model->apellidos  ,'cate'=>$_POST['categorias'] ,'productos'=>$this->producto_model->getAll(), 'categorias'=>$this->categorias_model->getAll(), 'cant_pro'=>$this->producto_model->userProd($this->user_model->id), 'cant_compras'=>$this->producto_model->userMont($this->producto_model->id)));
        }
	}
	
	/**
	 * Llama a la vista de producto con el producto seleccionado por el usuario para su compra 
	 */
	public function mostrar_producto($id)
	{
		$array = $this->usuariosesion->darUsuarioActual();
		$this->user_model->setUsuario($array->nusuario, $array->contrasenna);
		$this->load->view('mostrar_producto', array('usuario'=>$this->user_model->nombre." ".$this->user_model->apellidos ,'pro'=>$this->producto_model->getPro($id)));
	}

	/**
	 * Agrega el producto al carrito 
	 */
	public function agregar_carrito()
	{
		
		if (isset($_POST['aceptar'])) {
			$this->carrito_model->usuario= $this->idUser();
			$time = time();
			$this->carrito_model->fecha=date("d/m/Y H", $time);
			$this->carrito_model->producto=$_POST['id'];
			$prod=$this->producto_model->getPro($this->carrito_model->producto);
			$this->carrito_model->cantidad= $_POST['cant'];
			$this->carrito_model->descripcion=$prod[3];
			$this->carrito_model->precio=$this->carrito_model->cantidad*$prod[7];
			$res=$this->carrito_model->crear();
			if ($res) {
				header('location: /../user'); 
			}else{
				$array = $this->usuariosesion->darUsuarioActual();
			$this->user_model->setUsuario($array->nusuario, $array->contrasenna);
				$this->load->view('mostrar_producto', array('usuario'=>$this->user_model->nombre." ".$this->user_model->apellidos ,'error'=>"No se pudo agregar el producto a su carrito" ,'pro'=>$this->producto_model->getPro($id)));
			}
		}
	}

	/**
	 * Llama a  la vista del carrito con los datos del carrito del usuario actual
	 */
	public function ver_carrito()
	{
		$array = $this->usuariosesion->darUsuarioActual();
		$this->user_model->setUsuario($array->nusuario, $array->contrasenna);
		$this->load->view('carrito', array('usuario'=>$this->user_model->nombre." ".$this->user_model->apellidos , 'carritos'=>$this->carrito_model->getAll($this->idUser())));
	}

	/**
	 * Se encarga de mostrar las compras que ha realizado el usuario 
	 */
	public function compras()
	{
		$array = $this->usuariosesion->darUsuarioActual();
		$this->user_model->setUsuario($array->nusuario, $array->contrasenna);
		$productos=$this->carrito_model->get_ventas($this->user_model->id);
		$productos[]=array("xxxxx", "xxxxx", "xx/xx/xx", "xxxxx", "xxxxx", "xxxxx", "xxxxx");
		$fechas=array();
		$fech=explode(" ", $productos[0][2]);
		$monto=0;
		foreach ($productos as $key) {
			$temp=array();
			$t=$key[2];
			$temp_fech=explode(" ", $t);
			if ($temp_fech[0]==$fech[0]) {
				$monto+=$key[6];
			} else {
				$temp[]=array('fecha'=>$fech[0], 'monto'=>$monto);
				$fechas[]=$temp;
				$fech=$temp_fech;
				$monto=$key[6];
			}
		}

		$this->load->view('compras', array('usuario'=>$this->user_model->nombre." ".$this->user_model->apellidos , 'venta'=>$fechas, 'compras'=>$this->carrito_model->getAll($this->user_model->id)));
	}

	/**
	 * Retorna el id del usuario actual 
	 */
	public function idUser()
	{
		$array = $this->usuariosesion->darUsuarioActual();
		$this->user_model->setUsuario($array->nusuario, $array->contrasenna);
		return $this->user_model->id;
	}

	/**
	 * Se encarga de eliminar un producto del carrito 
	 */
	public function del_carrito($id)
	{
		$res=$this->carrito_model->eliminar($id);
		$array = $this->usuariosesion->darUsuarioActual();
		$this->user_model->setUsuario($array->nusuario, $array->contrasenna);
		if ($res) {
			$this->load->view('carrito', array('usuario'=>$this->user_model->nombre." ".$this->user_model->apellidos , 'carritos'=>$this->carrito_model->getAll($this->idUser())));
		}else {
			$this->load->view('carrito', array('usuario'=>$this->user_model->nombre." ".$this->user_model->apellidos , 'error'=>"No se pudo eliminar el producto a su carrito", 'carritos'=>$this->carrito_model->getAll($this->idUser())));
		}
	}

	/**
	 * realiza el checkout y la venta 
	 */
	public function checkout()
	{
		$array = $this->usuariosesion->darUsuarioActual();
		$this->user_model->setUsuario($array->nusuario, $array->contrasenna);
		$carrito=$this->carrito_model->getAll($this->idUser());
		$stock="";
		$checkout=true;
		$venta=true;
		foreach ($carrito as $key) {
			$pro=$this->producto_model->getPro($key[3]);
			if ($key[4] <= $pro[5]) {
				
			}elseif ($key[4] > $pro[5]) {
				$checkout=false;
				$stock+=$pro[2].", ";
			}
		}
		if ($checkout ) {
			foreach ($carrito as $val) {
				$res=$this->carrito_model->crear_venta($val);
				$pro=$this->producto_model->getPro($val[3]);
				$resto=$pro[6]-$val[4];
				$res1=$this->carrito_model->bajar_stock($val[3], $resto);
				$res=$this->carrito_model->eliminar($val[0]);
				if (!$res||!$res1) {
					$venta=false;
				}
			}

			$this->load->view('carrito', array('venta'=>$venta, 'modal'=>true, 'checkout'=>$checkout, 'usuario'=>$this->user_model->nombre." ".$this->user_model->apellidos , 'carritos'=>$this->carrito_model->getAll($this->idUser())));
		}elseif (!$checkout ) {
			$this->load->view('carrito', array('venta'=>$venta,'modal'=>true, 'stock'=>$stock, 'checkout'=>$checkout, 'usuario'=>$this->user_model->nombre." ".$this->user_model->apellidos , 'carritos'=>$this->carrito_model->getAll($this->idUser())));
		}
	}

	/**
	 * Según la fecha seleccionada mustra las compras efectuadas en dicha fecha 
	 */
	public function ver_compra($f, $fe, $fec)
	{
		$array = $this->usuariosesion->darUsuarioActual();
		$this->user_model->setUsuario($array->nusuario, $array->contrasenna);
		$productos=$this->carrito_model->get_ventas($this->user_model->id);
		$productos[]=array("xxxxx", "xxxxx", "xx/xx/xx", "xxxxx", "xxxxx", "xxxxx", "xxxxx");
		$fechas=array();
		$fech=explode(" ", $productos[0][2]);
		$monto=0;
		foreach ($productos as $key) {
			$temp=array();
			$t=$key[2];
			$temp_fech=explode(" ", $t);
			if ($temp_fech[0]==$fech[0]) {
				$monto+=$key[6];
			} else {
				$temp[]=array('fecha'=>$fech[0], 'monto'=>$monto);
				$fechas[]=$temp;
				$fech=$temp_fech;
				$monto=$key[6];
			}
		}

		$this->load->view('compras', array('fecha1'=>"$f/$fe/$fec", 'usuario'=>$this->user_model->nombre." ".$this->user_model->apellidos , 'venta'=>$fechas, 'compras'=>$productos));
	}

}
