<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
        $this->load->model('user_model');
		$this->load->model('producto_model');
		$this->load->model('categorias_model');
		$this->load->model('usuariosesion');
		$this->load->library('form_validation');
		$this->load->helper('url', 'form');
	}

	/**
	 * Esta función se encarga de primeramente verificar su el usuario es administrador, si no lo es se devuelve al controller principal,
	 *despues debe cargar la vista de administrador con sus datos para el dashboard 
	 */
	public function index()
	{
		$this->usuariosesion->_constructor();
		if (isset($_SESSION['usuario'])) {
			$array = $this->usuariosesion->darUsuarioActual();
			$this->user_model->setUsuario($array->nusuario, $array->contrasenna);
			if($this->user_model->admin==1){
                $n1=$this->user_model->cantidad_usuarios();
                $n2=$this->producto_model->cantVentas();
                $n3=$this->producto_model->montoVentas();
				$this->load->view('admin', array('cant_us'=>$n1, 'cant_ventas'=>$n2, 'monto'=>$n3));
			}else if ($this->user_model->admin==0) {
				header('location: principal');
			}
		} else {
			$this->load->view('login');
		}
	}

	/**
	 * Se encarga de traer las categorías de la base de datos y enviarlas al crud y levantar el mismo 
	 */
	public function verCategorias()
	{
		$cat['cat']=$this->categorias_model->getAll();
		$this->load->view('crud_categorias', $cat);
	}

	/**
	 * Se encarga de mandar a eliminar categorías 
	 */
	public function delCat($id)
	{
		$res=$this->categorias_model->eliminar($id);
		if ($res) {
			$this->load->view('crud_categorias', array('error'=>"Ocurrió un error al eliminar la categoría", 'cat'=>$this->categorias_model->getAll()));
		}else {
			$cat['cat']=$this->categorias_model->getAll();
			$this->load->view('crud_categorias', $cat);
		}
	}

	/**
	 * Manda por parametro el id seleccionado de una categoría para que se muestre en el campo y pueda ser editado
	 */
	public function upCat($id)
	{
		$this->load->view('crud_categorias', array('id'=>$id, 'cat'=>$this->categorias_model->getAll()));
	}

	/**
	 * Se encarga de tomar los datos de la actualización de la categoría y proceder a la actualización 
	 */
	public function updateCat()
	{
		if (isset($_POST['guardar'])) {
			$id = $_POST['id'];
			$nom = $_POST['nombre'];
			$res=$this->categorias_model->actualizar($id, $nom);
			if ($res) {
				$cat['cat']=$this->categorias_model->getAll();
				$this->load->view('crud_categorias', $cat);
			} else {
				$this->load->view('crud_categorias', array('error'=>"Ocurrió un error al actualizar la categoría", 'cat'=>$this->categorias_model->getAll()));
			}
		}
	}

	/**
	 * Recoge los datos de la nueva categoría y la crea
	 */
	public function crearCat()
	{
		if (isset($_POST['guardar'])) {
			$nom = $_POST['nombre1'];
			$res=$this->categorias_model->crear($nom);
			if ($res) {
				$cat['cat']=$this->categorias_model->getAll();
				$this->load->view('crud_categorias', $cat);
			} else {
				$this->load->view('crud_categorias', array('error'=>"Ocurrió un error al crear la categoría", 'cat'=>$this->categorias_model->getAll()));
			}	
		}
	}

	/**
	 * Se encarga de cargargar los datos de los productos, mandarlos a la vista y levantar la vista 
	 */
	public function verProductos()
	{
		$this->load->view('crud_productos', array('produc'=>$this->producto_model->getAll(), 'categorias'=>$this->categorias_model->getAll()));
	}

	/**
	 * Permite eliminar productos 
	 */
	public function delPro($id)
	{
		$res=$this->producto_model->eliminar($id);
		if ($res) {
			$this->load->view('crud_productos', array('error'=>"Ocurrió un error al eliminar el producto", 'cat'=>$this->producto_model->getAll()));
		}else {
			$this->load->view('crud_productos', array('produc'=>$this->producto_model->getAll(), 'categorias'=>$this->categorias_model->getAll()));
		}
	}

	/**
	 * Carga la vista con el id del producto a actualizar 
	 */
	public function upPro($id)
	{
		$this->load->view('crud_productos', array('id'=>$id, 'produc'=>$this->producto_model->getAll(), 'categorias'=>$this->categorias_model->getAll()));
	}

	/**
	 * Toma los datos del producto a actualizar y llama al proceso de actualizar 
	 */
	public function updatePro()
	{
		if (isset($_POST['guardar'])) {
			$id = $_POST['id'];
			$sku = $_POST['codigo'];
			$nom = $_POST['nombre'];
			$des = $_POST['descripcion'];
			$img = $this->loadImg($_FILES['imagen']);
			$catego = $_POST['categoria'];
			$stock = $_POST['stock'];
			$precio = $_POST['precio'];

			$res=$this->producto_model->actualizar($id, $sku, $nom, $des, $img, $catego, $stock, $precio);
			if ($res) {
				$produc['produc']=$this->producto_model->getAll();
				$this->load->view('crud_productos', $produc);
			} else {
				$this->load->view('crud_productos', array('error'=>"Ocurrió un error al actualizar el producto", 'produc'=>$this->producto_model->getAll(), 'categorias'=>$this->categorias_model->getAll()));
			}
		}
	}

	/**
	 * Toma los datos del nuevo producto y lo crea 
	 */
	public function crearPro()
	{
		if (isset($_POST['guardar'])) {
			$sku = $_POST['codigo1'];
			$nom = $_POST['nombre1'];
			$des = $_POST['descripcion1'];
			$img = $this->loadImg($_FILES['imagen1']);
			$catego = $_POST['categoria1'];
			$stock = $_POST['stock1'];
			$precio = $_POST['precio1'];

			$res=$this->producto_model->crear($sku, $nom, $des, $img, $catego, $stock, $precio);
			if ($res) {
				$produc['produc']=$this->producto_model->getAll();
				$this->load->view('crud_productos', $produc);
			} else {
				$this->load->view('crud_productos', array('error'=>"Ocurrió un error al guardar el producto", 'produc'=>$this->producto_model->getAll(), 'categorias'=>$this->categorias_model->getAll()));
			}
		}
	}

	/**
	 * se encarga de leer la imagen y tomar sus datos en crudo 
	 */
	public function loadImg($imagen)
	{
		$ruta_imagen=$imagen['tmp_name'];
        $nombre_imagen=$imagen['name'];
        $tamano_imagen=$imagen['size'];

        $ruta_destino=$_SERVER['DOCUMENT_ROOT']."/img/";

        if ($tamano_imagen<=1000000) {
            move_uploaded_file($ruta_imagen, $ruta_destino.$nombre_imagen);

            $fp = fopen($ruta_destino.$nombre_imagen, "r");
            $contenido = fread($fp, $tamano_imagen);
            $contenido = addslashes($contenido);
			fclose($fp);
			return $contenido;
		}
	}
}
