<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body style="width:100%; height:100%; overflow:hidden"><!-- -->
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="/../user" class="brand-logo">ESHOP  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><span><?php echo $usuario?></span></li>
                    <li><a href="/../user/carrito"><i class=" material-icons">shopping_cart</i></a></li>
                    <li><a href="/../user/ compras">Historial de compras</a></li>
                    <li><a href="/../logout" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
        </div>
        <div style="display:block; overflow-y:auto; height:90vh" class="col s10 yellow">
            <br>
            <br>
            <?php
                /**
                 * Se encarha de mostrar la existencia de algún error
                 */
                if (isset($error)) {
                    echo '<h4 style="color:red">$error</h4>';
                }    
            ?>
            <h4 style="margin:auto; text-align:center;">Carrito de compras</h4>
            <br>
            <br>
            <table>
                <thead>
                    <tr>
                        <th>producto</th>
                        <th>descripcion</th>
                        <th>cantidad</th>
                        <th>precio</th>
                        <th>eliminar</th>
                    </tr>
                 </thead>
            <tbody>
            <?php
                /**Muestra los productos del carrito del usuario actual */
                $total=0;
                foreach ($carritos as $key) {
                    echo '<tr>
                            <td><img width="100vw" src="data:image/jpg;base64,'.base64_encode($key[7]).'"/></td>
                            <td>'.$key[5].'</td>
                            <td>'.$key[4].'</td>
                            <td>₡'.$key[6].'</td>
                            <td><a style="color:#ef6c00; text-decoration: none;" href="/../user/delcarrito/'.$key[0].'" >ELIMINAR</a></td>
                        </tr>';
                    $total+=$key[6];
                } 
            ?> 
                <tr>
                    <td></td>
                    <td></td>
                    <td>TOTAL</td>
                    <td><?php  echo "₡".$total; ?></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <br>
        <br>
        <div style="margin:auto; display: flex; justify-content:flex-end;">
            <a class="btn waves-effect waves-light large btn-primary orange darken-3 " href="/../user/checkout">CONFIRMAR</a>
        </div>
        <?php 
            /**
             * Valida si se realizó el checkout y habilita un botón para mostrar el mensaje al usuario
             */
            if (isset($modal)) {
                echo '<br><br><div style="margin:auto; display: flex; justify-content:flex-end;">
                        <a data-target="modal1" class="btn waves-effect waves-light large btn-primary orange darken-3 modal-trigger" href="#modal">CONFIRMAR</a>
                    </div>';
            }
        ?>
        <div id="modal1" class="modal">
            <div class="modal-content">
                <h4>Checkout</h4>
                <span><?php
                /**
                 * Muestra un mensaje dependiendo de si se logró realizar el checkout
                 */
                if (isset($modal)) {
                    if ($modal) {
                        echo "La compra se ha realizado exitosamente";
                    }else if(!$modal){
                        echo "La compra NO se ha realizado exitosamente, por favor revise el nivel de disponibilidad de los siguientes productos: $stock";
                    }
                }

                ?></span>
            </div>
            <div class="modal-footer">
                <a href="" class="modal-close waves-effect waves-green btn-flat">X</a>
            </div>
        </div>
    </div>
</body>
</html>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
    $(document).ready(function(){
        $('.modal').modal();
  });

</script>