<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body style="width:100%; height:100%; overflow:hidden" >
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="user" class="brand-logo">ESHOP  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><span><?php echo $nom?></span></li>
                    <li><a href="/../user/carrito"><i class=" material-icons">shopping_cart</i></a></li>
                    <li><a href="/../user/compras">Historial de compras</a></li>
                    <li><a href="logout" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
            <br>
            <br>
            <div class="row">
                <form action="selectCat" method="POST">
                    <div class="input-field col s9">
                        <select name="categorias" style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge">
                            <option style="border-bottom: 1px solid #ef6c00; box-shadow: 0 1px 0 0 #ef6c00"><span style="color:#ef6c00">Marque una categoría</span></option>
                            <?php
                                /**
                                 * Llena el selector de las categorías
                                 */
                                foreach ($categorias as $valor) { 
                                    if (isset($cate) && $valor[0]==$cate) {
                                        echo '<option selected style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge" value="'.$valor[0].'"  ><span style="color:#ef6c00">'.$valor[1].'</span></option>';
                                    } else {
                                        echo '<option style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge" value="'.$valor[0].'"><span style="color:#ef6c00">'.$valor[1].'</span></option>';
                                    }
                                }
                            ?> 
                        </select>
                        <label>Categorías</label>
                    </div>
                    <div class="col s2"> 
                        <br>
                        <input type="submit" class="btn waves-effect waves-light orange darken-3" name="cat" style="width:3vw" value="🔍">
                    </div>  
                </form>
            </div>
            <div style="text-align:center;">
                <hr>
                <h5>Tus estadísticas</h5>
                <hr>
                <span>Total de productos</span>
                <hr>
                <span><?php echo $cant_pro?></span>
                <hr>
                <span>Monto comprado</span>
                <hr>
                <span><?php echo '₡ '.$cant_compras?> </span>
                <hr>
            </div>
        </div>
        <div style="display:block; overflow-y:auto; height:90vh" class="col s10 yellow">
            <div class="container" >
                <?php
                    /**
                     * Imprime la tabla de los productos de manera normal o si se escogió alguna categoría
                     */
                    if (isset($cate)) {
                        $cate=$cate;
                        foreach ($categorias as $val) {
                            if ($val[0] == $cate) {
                                echo '<h5>Categoría:" '.$val[1].' "</h5>';
                            }
                        }
                        echo '<table class=" responsive-table">
                                <tbody>';
                        $num1=0;
                        foreach ($productos as $key) {
                            if ($key[5]==$cate) {
                                if ($num1==0) {
                                    echo '<tr>
                                            <td>
                                                <div class="container">
                                                    <table class="centered responsive-table">
                                                        <tbody>
                                                            <tr style="height:20vh">
                                                                <td>
                                                                    <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key[4]).'"/>   
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <span>'.$key[2].'</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a class="waves-effect waves-light btn modal-trigger col s12" href="/../user/mostrar_producto/'.$key[0].'">Ver info.</a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>';
                                }elseif ($num1==1) {
                                    echo '<td>
                                            <div class="container">
                                                <table class="centered responsive-table">
                                                    <tbody>
                                                        <tr style="height:20vh">
                                                            <td>
                                                                <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key[4]).'"/>   
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>'.$key[2].'</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a class="waves-effect waves-light btn modal-trigger col s12" href="/../user/mostrar_producto/'.$key[0].'">Ver info.</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>';
                                }elseif ($num1==2) {
                                    echo '<td>
                                            <div class="container">
                                                <table class="centered responsive-table">
                                                    <tbody>
                                                        <tr style="height:20vh">
                                                            <td>
                                                                <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key[4]).'"/>   
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td>
                                                                <span>'.$key[2].'</span>
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td>
                                                                <a class="waves-effect waves-light btn modal-trigger col s12" href="/../user/mostrar_producto/'.$key[0].'">Ver info.</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                        <tr>';
                                    $num=0;
                                }
                                $num1++;
                            }
                        }
                        echo '</tbody>
                        </table>';
                                    
                    }else  {
                        echo '<h5>Todos los productos</h5>';

                        echo '<table class=" responsive-table">
                                <tbody>';
                        $num1=0;
                        if (isset($productos)) {
                            foreach ($productos as $key) {
                                $num1++;
                                if ($num1==1) {
                                    echo '<tr>
                                            <td>
                                                <div class="container">
                                                    <table class="centered responsive-table">
                                                        <tbody>
                                                            <tr style="height:20vh">
                                                                <td>
                                                                    <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key[4]).'"/>   
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <span>'.$key[2].'</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <a class="waves-effect waves-light btn modal-trigger col s12" href="/../user/mostrar_producto/'.$key[0].'">Ver info.</a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>';
                                }elseif ($num1==2) {
                                        echo '<td>
                                            <div class="container">
                                                <table class="centered responsive-table">
                                                    <tbody>
                                                        <tr style="height:20vh">
                                                            <td>
                                                                <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key[4]).'"/>   
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>'.$key[2].'</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a class="waves-effect waves-light btn modal-trigger col s12" href="/../user/mostrar_producto/'.$key[0].'">Ver info.</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>';
                                }elseif ($num1==3) {
                                        echo '<td>
                                                <div class="container">
                                                    <table class="centered responsive-table">
                                                        <tbody>
                                                            <tr style="height:20vh">
                                                                <td>
                                                                    <img width="100vw" src="data:image/jpg;base64,'.base64_encode($key[4]).'"/>   
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                <td>
                                                                    <span>'.$key[2].'</span>
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                <td>
                                                                    <a class="waves-effect waves-light btn modal-trigger col s12" href="/../user/mostrar_producto/'.$key[0].'">Ver info.</a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                            <tr>';
                                        $num1=0;
                                }
                                    
                            }
                        }else{
                            echo 'No hay productos';
                        }
                        echo '</tbody>
                        </table>';
                    }
                ?>
            </div>
        </div>
    </div>
</body>
</html>



<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
    /* select */
    $(document).ready(function(){
        $('select').formSelect();
    });

</script>
