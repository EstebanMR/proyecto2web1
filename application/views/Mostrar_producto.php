<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body style="width:100%; height:100%; overflow:hidden">
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="/../user" class="brand-logo">ESHOP  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><span><?php echo $usuario?></span></li>
                    <li><a href="/../user/carrito"><i class=" material-icons">shopping_cart</i></a></li>
                    <li><a href="/../user/compras">Historial de compras</a></li>
                    <li><a href="/../logout" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div style="" class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
        </div>
        <div style="height:90vh" class="col s10 yellow"  style="display:block; overflow-y:auto; height:90vh">
            <div class="col s1"></div>
            <div class="col s9">
                <div style="height:10vh">
                </div>
                <div style="display: flex;justify-content:center;">
                    <?php 
                        /**
                         * Muestra la imagen del producto seleccionado
                         */
                        echo '<img width="250vw" src="data:image/jpg;base64,'.base64_encode($pro[4]).'"/>';
                    ?>
                </div>
                <div>
                    <h4><?php echo $pro[2]?></h4>
                    <h6>SKU:<?php echo $pro[1]?></h6>
                    <br>
                    <h6>Descripción:</h6>
                    <p><?php echo $pro[3]?></p>
                </div>
            </div>
            <div class="col s2" style=" height:90vh">
                <br>
                <br>
                <br>
                <?php if (isset($error)) {
                    echo "<span>".$error."</span>";
                }?>
                <form action="/../user/agregar_carrito" method="POST">  
                    <label for="">Stock</label>
                    <input disabled type="text" name="stock" value="<?php echo $pro[6];?>">
                    <input style="visibility:hidden" name="id" type="text" value="<?php echo $pro[0];?>">
                    <label disabled for="">Precio en ₡</label>
                    <input disabled type="number" name="precio" value="<?php echo $pro[7]; ?>">
                    <label for="">Cantidad:</label>
                    <input required type="number" name="cant" value="1">
                    <br>
                    <input style="font-size:1vw" type="submit" class="btn waves-effect waves-light large btn-primary orange darken-3" name="aceptar" value="Agregar al carrito">
                </form>
            </div>
        </div>
    </div>
</body>
</html>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
