<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
	<div class="row" style="padding-top:1%; padding-bottom:1%">
        <div class="col s4"></div>
        <div class="col s4 valign-wrapper" style="height: 93vh; display: flex; flex-direction:column;">
            <br>
			<img style="border-radius: 150%; overflow: hidden;" src="img\user-icon-orange.jpg" width="200" height="200"> 
            <br>
            <div style=" height:57vh; border-radius:50px; border: black 0.5px solid; padding:5%; ">
                <h4>Iniciar sesión</h4>
                <br>
                <div class="p-2" >
                    <div style="color: red;">
                        <?php
                            /**
                             * Muestra si existe un error
                             */
                            if(isset($errorLogin)){
                                echo $errorLogin;
                            }
                        ?>
                    </div>
                </div>
                <form action="login" method="POST">
                    <div>
                        <label>Nombre de usuario</label>
                        <input type="text" name="nombre" style="width: 28vw; box-shadow: 0 1px 0 0 #ef6c00" placeholder="Nombre de usuario" required>
                        <br>
                        <label>Contraseña</label>
                        <input type="password" name="contra" style="width: 28vw; box-shadow: 0 1px 0 0 #ef6c00" placeholder="Contrasña" required>
                    </div>
                    <br>
                    <br>
                    <input type="submit" class="btn large btn-primary orange darken-3" value="Ingresar" name="enviar" style="width: 100%; border-radius:50px; color:black"></input>
                    <br>
                    <br>
                    <input type="submit" class="btn large btn-primary orange darken-3" value="Registrarse" name="registro" style="width: 100%; border-radius:50px; color:black"></input>
                </form>
                <br>
                <!-- <a class="btn large btn-primary orange darken-3" style="width: 100%; border-radius:50px; color:black" href="vistas\registro.php">Registrarse</a> -->
            </div>
        </div>
        <div class="col s4"></div>
    </div>
</body>
</html>