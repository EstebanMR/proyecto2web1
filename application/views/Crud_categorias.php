<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body style="width:100%; height:100%; overflow:hidden" >
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="/../admin" class="brand-logo">ESHOP <span style="font-size:small">DASHBOARD</span>  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <!--<li><a href="sass.html">Sass</a></li>
                    <li><a href="badges.html">Components</a></li>
                    <li><a href=""><i class="large material-icons">shopping_cart</i></a></li>-->
                    <li><a href="/../logout" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div style="" class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
            <br>
            <br>
            <br>
            <div class="row">
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="/../admin/categorias">Categorias</a>
                </div>
                <br>
                <br>
                <br>
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="/../admin/productos">Productos</a>
                </div>
            </div>
        </div>
        <div style="height:90vh" class="col s10 yellow ">
            <div class="col s5" style="height:90vh; display:block; overflow-y:auto; height:85vh; ">
                <div class="container" >
                    <h3 style="margin:auto; text-align:center;">Listado de productos</h3>
                    <br>
                    <table class="responsive-table">
                        <thead>
                            <tr>
                                <th>ID  </th>
                                <th>Categorías</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>    
                        <tbody>
                            <?php
                                /**
                                 * Imprime la tabla de las categorías
                                 */
                                if (isset($cat)) {
                                    $num=1;
                                    foreach ($cat as $value) {
                                        echo '<tr>
                                                <td>'.$num.'</td>
                                                <td>'.$value[1].'</td>
                                                <td><a style="color:#ef6c00; text-decoration: none;" href="/../admin/categoriasDel/'.$value[0].'" >ELIMINAR</a>  |  <a style="color:#ef6c00; text-decoration: none;" href="/../admin/categorias/'.$value[0].' " >EDITAR</a> </td>
                                            </tr>';
                                            $num++;
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col s7" style=" height:90vh">
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="container">
                    <h5>Categorías</h5>
                    <br>
                    <br>
                    <div>
                    <div class="p-2" >
                        <div style="color: red;">
                            <?php
                                if(isset($error)){
                                    echo $error;
                                }
                            ?>
                        </div>
                    </div>
                            <?php
                                /**Imprime el CRUD dependiendo si se va a crear o si de desea editar */
                                if (isset($id)) {
                                    $nom='';
                                    foreach ($cat as $key ) {
                                        if ($key[0]==$id) {
                                            $nom=$key[1];
                                        }
                                    }
                                    echo '<form action="../categoriasUp" method="POST">
                                            <input style="visibility:hidden" name="id" type="text" value="'.$id.'">
                                            <label for="name">Nombre de la categoría</label>
                                            <input style="background-color: #ffeb3b" name="nombre" id="nombre" type="text" value="'.$nom.'">';
                                }else{
                                    echo '<form action="../admin/categoriasCr" method="POST">
                                            <label for="name">Nombre de la categoría</label>
                                            <input style="background-color: #ffeb3b" name="nombre1" id="nombre" type="text">';
                                }
                            ?>
                            <br>
                            <br>
                            <br>
                            <input style="width: 100%;" class="btn large btn-primary orange darken-3" type="submit" value="Guardar" name="guardar">
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.