<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body style="width:100%; height:100%;" >
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="admin" class="brand-logo">ESHOP <span style="font-size:small">DASHBOARD</span>  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="logout" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div style="" class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
            <br>
            <br>
            <br>
            <div class="row">
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="admin/categorias">Categorias</a>
                </div>
                <br>
                <br>
                <br>
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="admin/productos">Productos</a>
                </div>
            </div>
        </div>
        <div style="height:90vh" class="col s10 yellow ">
            <div class="col s4 container" style="height:90vh; display: flex;align-items: center">
                <table class="centered responsive-table">
                    <thead>
                        <tr>
                            <th>
                                <h4>Cantidad de usuarios registrados</h4>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php
                                    /**
                                     * Muestra la cantidad de usuarios registrados
                                     */
                                    if (isset($cant_us)) {
                                        echo $cant_us;
                                    }
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col s4" style="height:90vh; display: flex;align-items: center">
            <table class="centered responsive-table">
                    <thead>
                        <tr>
                            <th>
                                <h4>Cantidad de productos vendidos</h4>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php
                                    /**
                                     * Muestra la cantidad de ventas realizadas
                                     */
                                    if (isset($cant_ventas)) {
                                        echo $cant_ventas;
                                    }
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col s4" style="height:90vh; display: flex;align-items: center">
            <table class="centered responsive-table">
                    <thead>
                        <tr>
                            <th>
                                <h4>Monto total de ventas en colones</h4>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php
                                /**
                                 * Muestra el monto gastado por los usuarios
                                 */
                                    if (isset($monto)) {
                                        echo $monto;
                                    }
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.