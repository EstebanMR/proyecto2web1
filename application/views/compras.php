<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body style="width:100%; height:100%; overflow:hidden"> <!--  -->
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <a href="/../user" class="brand-logo">ESHOP  <i class="large material-icons">desktop_windows</i></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><span><?php echo $usuario?></span></li>
                    <li><a href="/../user/carrito"><i class=" material-icons">shopping_cart</i></a></li>
                    <li><a href="/../user/compras">Historial de compras</a></li>
                    <li><a href="/../logout" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
        </div>
        <div style="display:block; overflow-y:auto; height:90vh" class="col s10 yellow">
            <div class="col s8"> 
                <br>
                <br>
                <h4 style="margin:auto; text-align:center;">Listado de compras</h4>
                <br>
                <br>
                <table class="container">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Total</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            /**
                             * Imprime la tabla con las diferentes compras realizadas por el usuario actual
                             */
                            if (isset($venta)) {
                                foreach ($venta as $key) {
                                    foreach ($key as $value) {
                                        echo '<tr>
                                                <td>'.$value['fecha'].'</td>
                                                <td>'.$value['monto'].'</td>
                                                <td><a style="color:#ef6c00; text-decoration: none;" href="/../user/vercompra/'.$value['fecha'].'" >VER</a></td>
                                            </tr>';
                                    }
                                }
                            } 
                                
                        ?> 
                    </tbody>
                </table>
                <br>
                <br>
 
            </div>
            <div class="col s4"> 
                <br>
                <br>
                <h4 style="margin:auto; text-align:center;">Listado de compras</h4>
                <br>
                <br>
                <h5><?php if (isset($fecha1)) {
                    echo $fecha1;
                }?></h5>
                <table class="container">
                    <thead>
                        <tr>
                            <th>Descripcion</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            /**
                             * Muestra los detalles de la compra realizada por el usuario actual en la fecha deseada
                             */
                            $total_pre=0;
                            if (isset($compras) && isset($fecha1)) {
                                foreach ($compras as $key) {
                                    $temp= explode(" ", $key[2]);
                                    if ($temp[0]==$fecha1) {
                                        echo '<tr>
                                                <td>'.$key[4].'</td>
                                                <td>'.$key[5].'</td>
                                                <td>'.$key[6].'</td>
                                            </tr>';
                                        $total_pre+=$key[6];
                                    } 
                                } 
                            }
                        ?>
                        <tr><td></td><td>Total</td><td><?php echo$total_pre;?></td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>