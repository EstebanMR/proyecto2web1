<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body style="width:100%; height:100%; overflow:hidden" >
    <div>
        <nav>
            <div style="padding-left:3%; padding-rigth:4%" class="nav-wrapper orange darken-3">
                <?php
                    if (isset($id)) {
                        echo '<a href="/.." class="brand-logo">ESHOP <span style="font-size:small">DASHBOARD</span>  <i class="large material-icons">desktop_windows</i></a>';
                    } else {
                        echo '<a href="../admin" class="brand-logo">ESHOP <span style="font-size:small">DASHBOARD</span>  <i class="large material-icons">desktop_windows</i></a>';
                    }
                    
                ?>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="/../logout" >Cerrar sesión</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div style="display: flex; flex-direction: row" class="row">
        <div style="" class="col s2 yellow lighten-5">
            <br>
            <br>
            <img width="170vw" style="display:block; margin:auto;" class="circle responsive-img z-depth-4" src="\img\shoppingcart.jpg">
            <br>
            <br>
            <br>
            <div class="row">
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="/../admin/categorias">Categorias</a>
                </div>
                <br>
                <br>
                <br>
                <div>
                    <div class="col s1"></div>
                    <a class="btn waves-effect waves-light orange darken-3 col s10" href="/../admin/productos">Productos</a>
                </div>
            </div>
        </div>
        <div style="height:90vh" class="col s10 yellow ">
            <div class="col s8">
                <div style="display:block; overflow-y:auto; height:90vh">
                    <h3 style="margin:auto; text-align:center;">Listado de productos</h3>
                    <br>
                    <table class=" centered responsive-table">
                        <thead>
                            <tr>
                                <th>ID  </th>
                                <th>SKU</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Imagen</th>
                                <th>Categoria</th>
                                <th>Stock</th>
                                <th>Precio</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>    
                        <tbody>
                            <?php
                            /**
                             * Imprime la tabla con los productos
                             */
                                if (isset($produc)) {
                                    foreach ($produc as $value) {
                                        echo '<tr>
                                                <td>'.$value[0].'</td>
                                                <td>'.$value[1].'</td>
                                                <td>'.$value[2].'</td>
                                                <td>'.$value[3].'</td>
                                                <td><img width="100vw" src="data:image/jpg;base64,'.base64_encode($value[4]).'"/></td>
                                                <td>'.$value[5].'</td>
                                                <td>'.$value[6].'</td>
                                                <td>'.$value[7].'</td>
                                                <td><a style="color:#ef6c00; text-decoration: none;" href="/../admin/productosDel/'.$value[0].'" >ELIMINAR</a>  |  <a style="color:#ef6c00; text-decoration: none;" href="/../admin/productos/'.$value[0].'" >EDITAR</a> </td>
                                            </tr>';
                                         
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col s4" style=" height:90vh">
                <div class="container" style="display:block; overflow-y:auto; height:90vh; width:25VW">
                    <h5>Productos</h5>
                    <div>
                            <?php
                                /**
                                 * Imprime el CRUD de los productos dependiendo de si se va a crear o a editar
                                 */
                                 if (isset($id)) {
                                    foreach ($produc as $prod) {
                                        if ($prod[0]==$id) {
                                            echo '<form action="/../admin/productosUp" method="POST" enctype="multipart/form-data">
                                                <input style="visibility:hidden" name="id" type="text" value="'.$prod[0].'">
                                                <label >SKU del producto</label>
                                                <input style="background-color: #ffeb3b" name="codigo" type="text" value="'.$prod[1].'">
                                                <label >Nombre del producto</label>
                                                <input style="background-color: #ffeb3b" name="nombre" type="text" value="'.$prod[2].'">
                                                <label >Descripcion del producto</label>
                                                <input style="background-color: #ffeb3b" name="descripcion"  type="text" value="'.$prod[3].'">
                                                <label >Imagen del producto</label>
                                                <br>
                                                <img width="170vw" src="data:image/jpg;base64,'.base64_encode($prod[4]).'"/>
                                                <br>
                                                <input style="background-color: #ffeb3b" name="imagen" type="file" REQUIRED>
                                                <br>
                                                <label >Categoria del producto</label>
                                                <select name="categoria" id="categoria" style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge">
                                                <option value="" style="border-bottom: 1px solid #ef6c00; box-shadow: 0 1px 0 0 #ef6c00"><span style="color:#ef6c00">Marque una categoría</span></option>';
                                                     
                                                foreach ($categorias as $valor) {
                                                    if ($valor[0]==$prod[5]) {
                                                        echo '<option selected style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge" value="'.$valor[0].'"><span style="color:#ef6c00">'.$valor[1].'</span></option>';
                                                    } else {
                                                        echo '<option style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge" value="'.$valor[0].'"><span style="color:#ef6c00">'.$valor[1].'</span></option>';
                                                        }
                                                    }
                                                        
                                                echo '</select><br>
                                                <label >Stock del producto</label>
                                                <input style="background-color: #ffeb3b" name="stock" type="text" value="'.$prod[6].'">
                                                <label >Precio del producto</label>
                                                <input style="background-color: #ffeb3b" name="precio" type="text" value="'.$prod[7].'">';
                                        }                                                                                
                                    } 
                                }else{
                                    echo '<form action="../admin/productosCr" method="POST" enctype="multipart/form-data">
                                    <label >Codigo del producto</label>
                                    <input style="background-color: #ffeb3b" name="codigo1" type="text" value="">
                                    <label >Nombre del producto</label>
                                    <input style="background-color: #ffeb3b" name="nombre1" type="text" value="">
                                    <label >Descripcion del producto</label>
                                    <input style="background-color: #ffeb3b" name="descripcion1"  type="text" value="">
                                    <label >Imagen del producto</label>
                                    <br>
                                    <img width="170vw" src="https://www.pequenomundo.cl/wp-content/themes/childcare/images/default.png"/>
                                    <br>
                                    <input style="background-color: #ffeb3b" name="imagen1" type="file" REQUIRED+>
                                    <br>
                                    <label >Categoria del producto</label>
                                    <select name="categoria1" id="categoria" style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge">
                                        <option value="" style="border-bottom: 1px solid #ef6c00; box-shadow: 0 1px 0 0 #ef6c00"><span style="color:#ef6c00">Marque una categoría</span></option>';
                                        
                                        foreach ($categorias as $valor) {
                                            echo '<option style="border-bottom: 1px solid organge; box-shadow: 0 1px 0 0 organge" value="'.$valor[0].'"><span style="color:#ef6c00">'.$valor[1].'</span></option>';
                                        }
                                         
                                    echo '</select><br>
                                    <label >Stock del producto</label>
                                    <input style="background-color: #ffeb3b" name="stock1" type="text" value="">
                                    <label >Precio del producto</label>
                                    <input style="background-color: #ffeb3b" name="precio1" type="text" value="">';
                                } 
                            ?>
                            <br>
                            <br>
                            <input style="width: 100%;" class="btn large btn-primary orange darken-3" type="submit" value="Guardar" name="guardar">
                        </form>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

<!-- materialize js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
    /* select */
    $(document).ready(function(){
        $('select').formSelect();
    });

</script>